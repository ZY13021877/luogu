## P5707 【深基2.例12】上学迟到 ##

**python 语言版**

```
import math
s,v=map(int,input().split())
n=8*60+24*60
t=math.ceil(s/v)+10
n=n-t
def f(a,b):
    if a<10:
        if b<10:
            print("0{}:0{}".format(a,b))
        else:
            print("0{}:{}".format(a,b))
    

    else:
        if b<10:
            print("{}:0{}".format(a,b))
        else:
            print("{}:{}".format(a,b))
if n>=24*60:
    n=n-24*60
    f(n//60,n%60)
else:
    f(n//60,n%60)

    
```

**c++语言版**

```
#include<bits/stdc++.h>
using namespace std;
double s,v;
int n,a,t,b;
int main()
{
	cin>>s>>v;
	n=8*60+24*60;//两天总共的分钟数
	t=ceil(s/v)+10;//ceil()很重要，向上取整，否则按C++逻辑会向下取整导致行走时间少。
	n=n-t;//得出剩下的时间。
	if(n>=24*60) 
	n-=24*60;
	b=n%60;//得出出发分。
	a=n/60;//得出出发时
	if(a<10)//慢慢判断是否补0
	{
		if(b<10) cout<<"0"<<a<<":0"<<b;
		else cout<<"0"<<a<<":"<<b;
	}
	else
	{
		if(b<10) cout<<a<<":0"<<b;
		else cout<<a<<":"<<b;
	}
	return 0;
}

```



--------------------------------------------------
## P3954 [NOIP2017 普及组] 成绩 ##

**python语言版**

```
a,b,c=map(int,input().split())
print(int(a*0.2+b*0.3+c*0.5))
```

**c语言版**

```
#include <stdio.h>
int main(){
    int a,b,c,d;
    scanf("%d %d %d",&a,&b,&c);
    d=a*0.2+b*0.3+c*0.5;
    printf("%d",d);
    return 0;
}
```



